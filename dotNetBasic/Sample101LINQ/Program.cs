﻿using Sample101LINQ.DataSource;

public class ProgramMain
{
    public static void Main()
    {
        AggregateOperator aggregate = new AggregateOperator();
        //aggregate.CountSyntax();
        //Console.WriteLine("Nested Count");
        //aggregate.CountNested();
        //aggregate.GroupedCount();
        aggregate.SumProjection();
        aggregate.CountConditional();
        aggregate.SumSyntax();
        aggregate.SumGrouped();
        aggregate.MinSyntax();
        aggregate.MinProjection();
        aggregate.MinGrouped();
        aggregate.MinEachGroup();
        aggregate.MaxSyntax();
        aggregate.MaxProjection();
        aggregate.MaxGrouped();
        aggregate.MaxEachGroup();
        aggregate.AverageSyntax();
        aggregate.AverageProjection();
        aggregate.AverageGrouped();
        aggregate.AggregateSyntax();
        aggregate.SeededAggregate();
    }
}