﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Sample101LINQ.DataSource
{
    public class AggregateOperator
    {
        public List<Product> GetProductList() => Products.ProductList;
        public List<Customer> GetCustomerList() => Customers.CustomerList;

        public int CountSyntax() 
        {
            #region CountSyntax
            int[] factors300 = { 2, 2, 5, 7, 7, 7, 1, 2, 4, 4, 7, 9, 3, 3, 5, 2, 5 };
            int distinct = factors300.Distinct().Count();

            Console.WriteLine($"Ada {distinct} angka unique dalam 'factors 300' ");
            #endregion
            return 0;

        }

        public int CountConditional()
        {
            #region count-conditional
            int[] numbers = { 5, 4, 1, 3, 9, 8, 6, 7, 2, 0 };

            int oddNumbers = numbers.Count(n => n % 2 == 1);

            Console.WriteLine("There are {0} odd numbers in the list.", oddNumbers);
            #endregion
            return 0;
        }

        public int CountNested() 
        {
            #region NestedCount
            
            List<Customer> customers = Customers.GetCustomerList();
            // cara 1
            var orderCounts = from c in customers
                              select (c.CustomerID, c.CompanyName, OrderCount: c.Orders.Count());


            foreach (var customer in orderCounts)
            {
                Console.WriteLine($"ID: {customer.CustomerID}, Name: {customer.CompanyName}, count: {customer.OrderCount}");
            }

            // cara 2
            //var orderCounts = from c in customers
            //                  select new
            //                  {
            //                      ID = c.CustomerID,
            //                      Name = c.CompanyName,
            //                      Count = c.Orders.Count()
            //                  };

            //foreach (var customer in orderCounts)
            //{
            //    Console.WriteLine($"ID: {customer.ID}, Name: {customer.Name}, count: {customer.Count}");
            //}


            #endregion
            return 0;
        }

        public int GroupedCount()
        {
            #region GroupedCount
            List<Product> products = GetProductList();

            var categoryCounts = from p in products
                                 group p by p.Category into g
                                 select (Category: g.Key, ProductCount: g.Count());
            foreach (var c in categoryCounts)
            {
                Console.WriteLine($"Category: {c.Category}: Product Count: {c.ProductCount}");
            }
            #endregion
            return 0;

        }
        public int SumSyntax()
        {
            #region SumSyntax
            int[] number = { 2, 4, 12, 2, 1, 2, 5, 7 };
            double numberSum = number.Sum();

            Console.WriteLine($"Totalnya adalah {numberSum}");
            #endregion
            return 0;

        }
        public int SumProjection()
        {
            #region SumOfProjection
            string[] words = { "manggo", "strawberry", "grape" };

            double totalChars = words.Sum(w => w.Length);

            Console.WriteLine($"Total {totalChars} karakter didalam kata tersebut.");
            #endregion
            return 0;
        }
        public int SumGrouped()
        {
            #region SumGrouped
            List<Product> products = GetProductList();

            var categories = from p in products
                             group p by p.Category into g
                             select (Category: g.Key, TotalUnitsInStock: g.Sum(p => p.UnitsInStock));

            foreach ( var pair in categories)
            {
                Console.WriteLine($"Category: {pair.Category}, Units in Stock: {pair.TotalUnitsInStock}");
            }
            #endregion
            return 0;
        }
        public int MinSyntax()
        {
            #region MinSyntax
            int[] numbers = { 2, 4, 6, 5, 6, 2, 3, 1, 6, 8 };

            int minNumber = numbers.Min();

            Console.WriteLine($"Angka minimum adalah {minNumber}");
            #endregion
            return 0;
        }
        public int MinProjection()
        {
            string[] words = { "manggo", "strawberry", "grape" };

            int shortestWords = words.Min(w => w.Length);
            Console.WriteLine($"Kalimat terpendek adalah {shortestWords}");
            return 0;
        }
        public int MinGrouped()
        {
            List<Product> products = GetProductList();

            var categories = from p in products
                             group p by p.Category into g
                             select (Category: g.Key, CheapestProduct: g.Min(p => p.UnitPrice));

            foreach (var c in categories)
            {
                Console.WriteLine($"Category: {c.Category}, Lowest Price: {c.CheapestProduct}");
            }
            return 0;
        }
        public int MinEachGroup()
        {
            List<Product> products = GetProductList();

            var categories = from p in products
                             group p by p.Category into g
                             let minPrice = g.Min(p => p.UnitPrice)
                             select (Category: g.Key, CheapestProduct: g.Where(p => p.UnitPrice == minPrice));

            foreach (var c in categories)
            {
                Console.WriteLine($"Category: {c.Category}");
                foreach (var p in c.CheapestProduct)
                {
                    Console.WriteLine($"\t Product : {p}");
                }
            }
            return 0;
        }

        public int MaxSyntax()
        {
            int[] numbers = { 8, 4, 8, 0, 4, 5, 7, 1, 3, 5 };
            int maxNumber = numbers.Max();

            Console.WriteLine($"Angka terbesar adalah {maxNumber}");

            return 0;
        }

        public int MaxProjection()
        {
            string[] words = { "manggo", "strawberry", "grape" };

            int longestWords = words.Max(w => w.Length);

            Console.WriteLine($"Kalimat terpanjang adalah {longestWords}");
            return 0;
        }
        public int MaxGrouped()
        {
            List<Product> products = GetProductList();

            var categories = from p in products
                             group p by p.Category into g
                             select (Category: g.Key, MostExpensivePrice: g.Max(p => p.UnitPrice));

            foreach (var c in categories)
            {
                Console.WriteLine($"Category: {c.Category}, product termahal adalah: {c.MostExpensivePrice}");
            }
            return 0;
        }
        public int MaxEachGroup()
        {
            List<Product> products = GetProductList();

            var categories = from p in products
                             group p by p.Category into g
                             let maxPrice = g.Max(p => p.UnitPrice)
                             select (Category: g.Key, MostExpensiveProduct: g.Where(p => p.UnitPrice == maxPrice));

            foreach (var c in categories)
            {
                Console.WriteLine($"Category: {c.Category}");
                foreach (var p in c.MostExpensiveProduct)
                {
                    Console.WriteLine($"\t{p}");
                }
            }
            return 0;
        }

        public int AverageSyntax()
        {
            int[] numbers = { 2, 4, 6, 2, 6, 2, 8, 0, 4, 6, 2 };

            double averageNumbs = numbers.Average();

            Console.WriteLine($"Rata-ratanya adalah: {averageNumbs}");
            return 0;
        }

        public int AverageProjection()
        {
            string[] words = { "manggo", "strawberry", "grape" };

            double averageProjection = words.Average(w => w.Length);

            Console.WriteLine($"Rata-rata panjang kata adalah: {averageProjection}");
            return 0;
        }

        public int AverageGrouped()
        {
            List<Product> products = GetProductList();

            var categories = from p in products
                             group p by p.Category into g
                             select (Category: g.Key, AveragePrice: g.Average(p => p.UnitPrice));

            foreach (var c in categories)
            {
                Console.WriteLine($"Category: {c.Category}, Average Price: {c.AveragePrice}");
            }
            return 0;
        }

        public int AggregateSyntax()
        {
            double[] doubles = { 1.2, 5.7, 2.4, 5.7, 2.4 };

            double product = doubles.Aggregate((runningProduct, nextFactor) => runningProduct * nextFactor);

            Console.WriteLine($"Total product dari semua angka: {product}");

            return 0;
        }

        public int SeededAggregate()
        {
            double startBalance = 100.0;

            int[] attemptsWithdrawals = { 20, 50, 30, 40, 70 };

            double endBalance =
                attemptsWithdrawals.Aggregate(startBalance,
                (balance, nextWithdrawal) =>
                    ((nextWithdrawal <= balance) ? (balance - nextWithdrawal) : balance));

            Console.WriteLine($"Balance terakhir: {endBalance}");

            return 0;
        }
    }
}
