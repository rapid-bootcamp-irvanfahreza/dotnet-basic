﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogicDasar
{
    public class Logic02Soal02
    {
        public Logic02Soal02() 
        {
        }
        public static void Cetak(int n)
        {
            for (int i = 0; i < n; i++)
            {
                int angka = 1;
                int akhir = 17;
                for (int j = 0; j < n; j++)
                {
                    if(i<=j && i==0 || i>=j && i == 8)
                    {
                        Console.Write(angka + "\t");
                        angka += 2;
                    }
                    else if(j<=i && j==0)
                    {
                        Console.Write(angka + "\t");
                    }
                    else if (j >= i & j == 8)
                    {
                        Console.Write(akhir + "\t");
                    }
                    else
                    {
                        Console.Write("\t");
                    }
                }
                Console.WriteLine("\n");
            }
        }
    }
}
