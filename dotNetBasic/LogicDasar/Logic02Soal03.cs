﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogicDasar
{
    public class Logic02Soal03
    {
        public Logic02Soal03() 
        {
        }
        public static void Cetak(int n)
        {
            for (int i = 0; i < n; i++)
            {
                int angka = 0;
                int akhir = 16;
                for (int j = 0; j < n; j++)
                {
                    if (i >= j && j == 0)
                    {
                        Console.Write(angka + "\t");
                    }
                    else if (i <= j && i == 0 || i >= j && i == 8 ||
                        i == j || i+j == n-1)
                    {
                        Console.Write(angka + "\t");
                    }
                    else if (i <= j && j == 8)
                    {
                        Console.Write(akhir + "\t");
                    }
                    else
                    {
                        Console.Write("\t");
                    }
                    angka += 2;
                }
                Console.WriteLine("\n");
            }
        }
    }
}
