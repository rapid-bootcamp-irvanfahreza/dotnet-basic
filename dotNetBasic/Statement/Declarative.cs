﻿public class Declarative
{
    public static void SampleDeclarative()
    {
        System.Console.WriteLine("Declarative Test");

        //Variable declarations statements
        double area = 0.0;
        Console.WriteLine("Area Value:" + area);
        double radius = 2;
        Console.WriteLine("Radius Value:" + radius);

        //Constant declarations statements
        const double phi = 3.14159;
        Console.WriteLine("Pi Value: " + phi);



    }
}