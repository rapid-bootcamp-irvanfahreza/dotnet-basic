﻿using DataTypes;
using DataTypes.CustomType;
using DataTypes.InheritanceType;
using DataTypes.ObjectType;
using DataTypes.OOP;
using DataTypes.PolymorphismType;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataType
{
    public struct Coords
    {
        public int x, y;
        public Coords(int p1, int p2)
        {
            x = p1;
            y = p2;
        }

        public override string ToString()
        {
            return "Coords (" + x + ", " + y + ")";
        }
    }
    public class MainDataType
    {
        public static void Mains() // kalo mau running, ganti Mains dengan Main
        {
            // main untuk SampleCoords
            //Coords point1 = new Coords(2, 5);
            //Console.WriteLine("Point1: " + point1);
            //    Coords point2 = new Coords(5, 5);
            //Console.WriteLine("Point2: " + point2);

            //SampleEnum();
            //SampleClass();
            //BankAccountSample();
            //TransactionBankAccount();
            //DifferentBankAccount();
            //SampleObject();
            //SampleInheritance();

            // cara lain call interface implement
            //Shape.SamplePolymorphism();
            SampleClassImplement.InterfaceImplement(); 
        }

        #region Sample Polymorphism

        #endregion

        #region Sample Interface

        #endregion

        #region Sample Inheritance
        public static void SampleInheritance()
        {
            // Create an instance of WorkItem by using the constructor in the
            // base class that takes three arguments.
            WorkItem item = new WorkItem("Fix Bugs",
                                        "Fix all bugs in my code branch",
                                        new TimeSpan(3, 4, 0, 0));

        // Create an instance of ChangeRequest by using the constructor in
        // the derived class that takes four arguments.
        ChangeRequest change = new ChangeRequest("Change Base Class Design",
                                                "Add members to the class",
                                                new TimeSpan(4, 0, 0),
                                                1);

        // Use the ToString method defined in WorkItem.
        Console.WriteLine(item.ToString());

            // Use the inherited Update method to change the title of the
            // ChangeRequest object.
            change.Update("Change the Design of the Base Class",
                new TimeSpan(4, 0, 0));

            // ChangeRequest inherits WorkItem's override of ToString.
            Console.WriteLine(change.ToString());
        }

        #endregion

        #region Sample Object
    public static void SampleObject()
        {
            Person person1 = new Person("Irvan", 23);
            Console.WriteLine("person1 Name = {0} Age = {1}", person1.Name, person1.Age);

            // Deklarasi new "person", assign person 1 ke person 2
            Person person2 = person1;

            // Mengubah nama person 2 dan juga person 1 
            person2.Name = "Reza";
            person2.Age = 20;

            Console.WriteLine("person2 Name = {0} Age = {1}", person2.Name, person2.Age);
            Console.WriteLine("person1 Name = {0} Age = {1}", person1.Name, person1.Age);

            Console.WriteLine("\n");

            Console.WriteLine("Product");

            Products product1 = new Products("Coca-cola", "Minuman bersoda", "Minuman");
            Console.WriteLine("Product Name = {0}, Description = {1}, Category = {2}", product1.Name, product1.Description, product1.Category);


        }
       
        #endregion

        #region Sample Different type Account
        public static void DifferentBankAccount()
        {
            var giftCard = new GiftCardAccount("gift card", 100, 50);
            giftCard.MakeWithdrawal(20, DateTime.Now, "get expensive coffee");
            giftCard.MakeWithdrawal(50, DateTime.Now, "buy groceries");
            giftCard.PerformMonthEndTransactions();
            // can make additional deposits:
            giftCard.MakeDeposit(27.50m, DateTime.Now, "add some additional spending money");
            //Console.WriteLine(giftCard.GetAccountHistory());

            var savings = new InterestEarningAccount("savings account", 10000);
            savings.MakeDeposit(750, DateTime.Now, "save some money");
            savings.MakeDeposit(1250, DateTime.Now, "Add more savings");
            savings.MakeWithdrawal(250, DateTime.Now, "Needed to pay monthly bills");
            savings.PerformMonthEndTransactions();
            //Console.WriteLine(savings.GetAccountHistory());

            var lineOfCredit = new LineOfCreditAccount("line of credit", 0, 2000);
            // How much is too much to borrow?
            lineOfCredit.MakeWithdrawal(1000m, DateTime.Now, "Take out monthly advance");
            lineOfCredit.MakeDeposit(50m, DateTime.Now, "Pay back small amount");
            lineOfCredit.MakeWithdrawal(5000m, DateTime.Now, "Emergency funds for repairs");
            lineOfCredit.MakeDeposit(150m, DateTime.Now, "Partial restoration on repairs");
            lineOfCredit.PerformMonthEndTransactions();
            //Console.WriteLine(lineOfCredit.GetAccountHistory());
        }


        #endregion

        #region Sample Transaction Bank
        public static void TransactionBankAccount()
        {
            var account = new BankAccount("Irvan", 1000);
            Console.WriteLine($"Account {account.Number} was created for {account.Owner} with {account.Balance} initial balance.");
            // Test for a negative balance.
            try
            {
                account.MakeWithdrawal(750, DateTime.Now, "Attempt to overdraw");
            }
            catch (InvalidOperationException e)
            {
                Console.WriteLine("Exception caught trying to overdraw");
                Console.WriteLine(e.ToString());
            }

            try
            {
                account.MakeWithdrawal(750, DateTime.Now, "Attempt to overdraw");
            }
            catch (InvalidOperationException e)
            {
                Console.WriteLine("Exception caught trying to overdraw");
                Console.WriteLine(e.ToString());
            }

            // Test balance harus positif
            BankAccount invalidAccount;
            try
            {
                invalidAccount = new BankAccount("invalid", -55);
            }
            catch (ArgumentOutOfRangeException e)
            {
                Console.WriteLine("Exception caught creating account with negative balance");
                Console.WriteLine(e.ToString());
                return;
            }
        }

        #endregion

        #region Sample BankAccount
        public static void BankAccountSample()
        {
            var account = new BankAccount("Irvan", 2500000);
            Console.WriteLine($"Account {account.Number} was created for {account.Owner} with {account.Balance} initial balance.");
            var account1 = new BankAccount("Fahreza", 3500000);
            Console.WriteLine($"Account {account1.Number} was created for {account1.Owner} with {account1.Balance} initial balance.");
            var account2 = new BankAccount("Reza", 2000000);
            Console.WriteLine($"Account {account2.Number} was created for {account2.Owner} with {account2.Balance} initial balance.");
        }
        #endregion

        #region Sample Class
        public static void SampleClass()
        {
            SampleClass sampleClass;

            sampleClass = new SampleClass();
            Console.WriteLine("After call constructor");
            Console.WriteLine("Sample class value: {0}", sampleClass.ToString());

            sampleClass = new SampleClass(5, 7);
            Console.WriteLine("After call constructor");
            Console.WriteLine("Sample class value: {0}", sampleClass.ToString());
        }

        #endregion

        #region Sample Enum
        public static void SampleEnum()
        {
            Type weekDays = typeof(EnumDays);
            foreach (var item in Enum.GetNames(weekDays))
            {
                Console.WriteLine("Days: {0}", item);
            }

            Type fieldModel = typeof(EnumFileMode);
            foreach (var item in Enum.GetNames(fieldModel))
            {
                Console.WriteLine("Field Model: {0}", item);
            }
            Colors myColors = Colors.Red | Colors.Blue | Colors.Yellow;
            Console.WriteLine();
            Console.WriteLine("myColors holds a combination of colors. Namely: {0}", myColors);
            Console.WriteLine("Color Red: {0}, GetName: {1}", Colors.Red, Enum.GetName(Colors.Red));

            Console.WriteLine("Status Approved: {0}, Names: {1}", ApprovalStep.Approved, Enum.GetName(ApprovalStep.Approved));

        }

        #endregion

        #region SampleCoord
        public static void SampleCoord()
        {

        }
        #endregion

        #region Data Type
        public static void SampleQuery()
        {
            char firstLetter = 'C';
            var limit = 2;
            int[] source = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 };
            //LINQ
            var query1 = from item in source
                         where item <= limit
                         select item;
            Console.WriteLine("Query result: " + query1);
            foreach (var item in query1)
            {
                Console.WriteLine("Item Value: " + item);
            }

            var query2 = from item in source
                         where item % 2 == 1
                         select item;
            Console.WriteLine("Odd result: ");
            foreach (var item in query2)
            {
                Console.WriteLine("Item Value: " + item);
            }

            var query3 = from item in source
                         where item % 2 == 0
                         select item;
            Console.WriteLine("Odd result: ");
            foreach (var item in query3)
            {
                Console.WriteLine("Item Value: " + item);
            }
        }
        #endregion
        }
}