﻿using LINQ;
using LINQ.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


public class ProgramMain
{
    public static void Main()
    {
        //IntroLINQ.Introduction();
        //IntroLINQ.IntroductionWithLinq();
        Product.SampleFilterProduct();
        Product.SampleFilterName();
        Category.SampleFilterCategory();
    }

    #region Introduction
    public static void IntroductionLinq()
    {
        Console.WriteLine("Introduction without LinQ : ");
        IntroLINQ.Introduction();
        Console.WriteLine("\n Intro with Linq");
        IntroLINQ.IntroductionWithLinq();
    }
    #endregion

    #region Sample Product

    #endregion

}


