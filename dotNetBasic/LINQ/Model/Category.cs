﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINQ.Model
{
    public class Category
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Category(int id, string name, string description)
        {
            this.Id = id;
            this.Name = name;
            this.Description = description;
        }

        public override string ToString()
        {
            return "Categories {Name : " + this.Name + ", Description: " + this.Description + "}";
        }

        public static List<Category> GetData()
        {
            List<Category> categories = new List<Category>()
            {
                new Category(1, "Makanan", "Makanan bisa dikunyah"),
                new Category(2, "Minuman", "Bisa di telan tanpa dikunyah")
            };
            return categories;
        }

        public static void SampleFilterCategory()
        {
            List<Category> categories = GetData();

            IEnumerable<Category> categoriesFilter =
                from item in categories
                where item.Id == 1
                select item;

            foreach (var category in categoriesFilter)
            {
                Console.WriteLine(category);
            }
        }
    }
}
