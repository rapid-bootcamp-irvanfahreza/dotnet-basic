﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINQ.Model
{
    public class Product
    {
        public string Name { get; set; }
        public int Price { get; set; }

        public Product(string name, int price)
        {
            this.Name = name;
            this.Price = price;

        }

        public override string ToString() 
        {
            return "Product { Name : " + this.Name + ", Price: " + this.Price + "}";
        }

        public static List<Product> GetData()
        {
            List<Product> products = new List<Product>()
            {
                new Product("Ayam Goreng", 15000),
                new Product("Sosis", 5000),
                new Product("Bakso", 10000)
            };
            return products;
        }

        public static void SampleFilterProduct()
        {
            // 1. Create data source
            List<Product> products = GetData();

            // 2. Create query
            IEnumerable<Product> productFilter =
                from item in products
                where item.Price >= 10000
                select item;

            // 3. Execute query
            foreach (var product in productFilter)
            {
                Console.WriteLine(product);
            }
        }

        public static void SampleFilterName()
        {
            List<Product> products = GetData();

            IEnumerable<Product> productFilter =
                from item in products
                where item.Name.Contains("Bakso")
                select item;

            foreach (var product in productFilter)
            {
                Console.WriteLine(product);
            }
        }
    }
}
